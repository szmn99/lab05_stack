package com.company;

public class Main {

    public static void main(String[] args) {

        StackOperations test = new Stack();

        test.push("Samsung");
        test.push("PNY");
        test.push("Kingston");
        test.push("Sony");

        System.out.println(test.get());

        test.pop();

        System.out.println(test.get());
    }
}
